#include <IRremote.h>
 
int pin_recept = 11; // On définit le pin 11  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage données reçues
const int led = 13;
//int etatLed;
 
void setup() 
{ 
  Serial.begin(9600); 
  ir_recept.enableIRIn(); // Initialisation de la réception 
  pinMode(led, OUTPUT);
  int led = LOW;
}
 
void loop() 
{ 
  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
    /*if (ir_decode.value == 0xA23D48B7){
      digitalWrite(led, HIGH);
      //etatLed = HIGH;
    }
    if (ir_decode.value == 0xA23D22DD){    
      digitalWrite(led, LOW);
      //etatLed = LOW;     
    }*/
    if (ir_decode.value == 0xA23D23DC){    
      digitalWrite(led, LOW);
      delay(100);
      digitalWrite(led, HIGH);
      delay(100);
      //etatLed = LOW;     
    }
  } 
}

//A23D22DD
//A23D48B7

