// déclaration des broches utilisées
const char ledDroite = 2;
const char ledGauche = 3;
const char capteurDroit = 0;
const char capteurGauche = 1;

/* deux variables par capteur qui une stockera la valeur lue sur la broche analogique
et l'autre stockera le résultat de la conversion de la précédente valeur en tension */
float lectureDroite = 0;
float lectureGauche = 0;
float tensionDroite = 0;
float tensionGauche = 0;

void setup()
{
    pinMode(ledDroite, OUTPUT);
    pinMode(ledGauche, OUTPUT);
    Serial.begin(9600);
}

void loop()
{
    // lecture de la valeur en sortie du capteur capteurDroit puis gauche
    lectureDroite = analogRead(capteurDroit);
    lectureGauche = analogRead(capteurGauche);
    // conversion  en tension de la valeur lue
    tensionDroite = (lectureDroite * 5.0) / 1024;
    tensionGauche = (lectureGauche * 5.0) / 1024;

    // si la tension lue en sortie du capteur 1 est plus grande
    // que celle en sortie du capteur 2
    if(tensionDroite > tensionGauche)
    {
        digitalWrite(ledDroite, LOW); // allumée
        digitalWrite(ledGauche, HIGH); // éteinte
    }
    else
    {
        digitalWrite(ledDroite, HIGH); // éteinte
        digitalWrite(ledGauche, LOW); // allumée
    }
    // envoi des données lues vers l'ordinateur
    Serial.print("Tension Droite = ");
    Serial.print(tensionDroite);
    Serial.println(" V");
    Serial.print("Tension Gauche = ");
    Serial.print(tensionGauche);
    Serial.println(" V");

    delay(100); // délai pour ne prendre une mesure que toutes les 100ms
}
