const char led1 = 2;  
const char led2 = 3;
const char capteur1 = A0;
const char capteur2= A1;

float tension1 = 0;  
float tension2 = 0;

void setup()
{
    // définition des broches utilisées
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    Serial.begin(9600); // la voie série pour monitorer
}

void loop()
{
    // conversion de cette valeur en tension
    tension1 = (analogRead(capteur1) * 5.0) / 1024;
    tension2 = (analogRead(capteur2) * 5.0) / 1024;

    if(tension1 > tension2)
    {
        digitalWrite(led1, LOW); 
        digitalWrite(led2, HIGH); 
    }
    if(tension2 > tension1)
    {
        digitalWrite(led2, LOW); 
        digitalWrite(led1, HIGH); 
    }
    /*else 
    {
        digitalWrite(led2, LOW); 
        digitalWrite(led1, LOW); 
    }*/
    // envoie de la valeur de la tension lue
    // vers l'ordinateur via la liaison série
    Serial.print("Tension 1 = ");
    Serial.print(tension1);
    Serial.println(" V");
    Serial.print("Tension 2 = ");
    Serial.print(tension2);
    Serial.println(" V");


    // délai pour ne prendre des mesures que toutes les demi-secondes
    delay(500);
}

