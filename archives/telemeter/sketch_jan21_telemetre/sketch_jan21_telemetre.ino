#define VITESSE 340

const int USTrig = 8; // Déclencheur sur la broche 8
const int USEcho = 9; // Réception sur la broche 9

void setup() {
    pinMode(USTrig, OUTPUT);
    pinMode(USEcho, INPUT);

    digitalWrite(USTrig, LOW);

    Serial.begin(9600);
}

void loop() {
  digitalWrite(USTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(USTrig, LOW);
  
  unsigned long duree = pulseIn(USEcho, HIGH);
  duree = duree/2;
  float distance = VITESSE*duree/100;
  Serial.println(distance);
  delay(200);
}

