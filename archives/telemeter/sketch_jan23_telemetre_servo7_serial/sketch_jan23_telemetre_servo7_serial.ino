#define VITESSE 340

const int USTrig = 8; // Déclencheur sur la broche 8
const int USEcho = 9; // Réception sur la broche 9

#include <Servo.h> 
 
Servo servo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 

void setup() {
    pinMode(USTrig, OUTPUT);
    pinMode(USEcho, INPUT);
    servo.attach(6);  // attaches the servo on pin 6 to the servo object 

    digitalWrite(USTrig, LOW);
    Serial.begin(9600);

}

void loop()
{
   // 1. Un état haut de 10 microsecondes est mis sur la broche "Trig"
   digitalWrite(USTrig, HIGH);
   delayMicroseconds(10); //on attend 10 µs
   // 2. On remet à l’état bas la broche Trig
   digitalWrite(USTrig, LOW);
   // 3. On lit la durée d’état haut sur la broche "Echo"
   unsigned long duree = pulseIn(USEcho, HIGH);
   // 4. On divise cette durée par deux pour n'avoir qu'un trajet
   duree = duree/2;
   // 5. On calcule la distance avec la formule d=v*t
   float temps = duree/1000.0; //on met en secondes 10000 = cm ; 1000 = mm, etc.
   float distance = temps*VITESSE; //on multiplie par la vitesse, d=t*v

   if((distance <=200)&&(pos<=0)){
                                     // in steps of 1 degree 
            servo.write(179);              // tell servo to go to position in variable 'pos' 
            pos = 179;
            Serial.println(distance);
            Serial.println(servo.read());
            delay(15);              // waits 15ms for the servo to reach the position 
      
   }
   else if((distance <=200)&&(pos>=179)){
                                      
            servo.write(0);              // tell servo to go to position in variable 'pos' 
            pos = 0;
            Serial.println(distance);
            Serial.println(servo.read());
            delay(15);                       // waits 15ms for the servo to reach the position 
      
   }
   pos = pos;
   //servo.write(pos);
   //delay(100);
}

