#include <AFMotor.h>
#define VITESSE_SON 340

const int USTrig = A1; // Déclencheur sur la broche 8
const int USEcho = A0; // Réception sur la broche 9

AF_DCMotor motor3(3, MOTOR34_1KHZ);
AF_DCMotor motor4(4, MOTOR34_1KHZ);
//create motor #2, 64KHz pwm

void setup()
{
  
Serial.begin(9600);
// set up Serial library at 9600 bps
//motor3.setSpeed(255);
motor3.setSpeed(100);
motor4.setSpeed(100);
// set the speed to 255

pinMode(USTrig, OUTPUT);
pinMode(USEcho, INPUT);
digitalWrite(USTrig, LOW);

}

void loop()
{
   // 1. Un état haut de 10 microsecondes est mis sur la broche "Trig"
   digitalWrite(USTrig, HIGH);
   delayMicroseconds(10); //on attend 10 µs
   // 2. On remet à l’état bas la broche Trig
   digitalWrite(USTrig, LOW);
   // 3. On lit la durée d’état haut sur la broche "Echo"
   unsigned long duree = pulseIn(USEcho, HIGH);
   // 4. On divise cette durée par deux pour n'avoir qu'un trajet
   duree = duree/2;
   // 5. On calcule la distance avec la formule d=v*t
   float temps = duree/1000.0; //on met en secondes 10000 = cm ; 1000 = mm, etc.
   float distance = temps*VITESSE_SON; //on multiplie par la vitesse, d=t*v

   if(distance <=200) 
   {
     motor3.run(BACKWARD);
     motor4.run(BACKWARD);  
     delay(500);
     motor3.run(FORWARD);
     delay(500);
   }
   else
   {
     motor3.run(FORWARD);
     motor4.run(FORWARD);
   }
} 
