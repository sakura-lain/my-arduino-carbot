/*TOSHIBA :

Power : 0xA23D48B7

BOUTONS CENTRAUX :
Enter : 0xA23D22DD
Up : 0xA23D03FC
Down : 0xA23D13EC
Left : 0xA23D33CC
Right : 0xA23D23DC

BOUTONS PERIPHERIQUES HAUTS :
SlowL : 0xA23D31CE
SlowR : 0xA23D11EE
SkipL : 0xA23D21DE
SkipR : 0xA23D01FE

BOUTONS PERIPHERIQUES BAS :
Frame : 0xA23D7986
Adjust : 0xA23DB946
PictureL : 0xA23D19E6
PictureR : 0xA23D59A6
*/

#include <AFMotor.h>
#include <IRremote.h>

AF_DCMotor motor3(3, MOTOR34_1KHZ);
AF_DCMotor motor4(4, MOTOR34_1KHZ);
//create motor #2, 64KHz pwm
 
int pin_recept = 3; // On définit le pin 3  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage données reçues
int vitesse;


void setup(){
  
Serial.begin(9600);
//motor3.setSpeed(255);
//motor4.setSpeed(255);

ir_recept.enableIRIn(); // Initialisation de la réception 
}

void loop()
{
  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~       
    //Marche avant :
    if (ir_decode.value == 0xA23D03FC){
      Serial.print("forward");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
    }
    //Marche arrière :
    if (ir_decode.value == 0xA23D13EC){
      Serial.print("backward");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
    }
    //Stop :
    if (ir_decode.value == 0xA23D22DD){
      Serial.print("stop");
      motor3.run(RELEASE);
      motor4.run(RELEASE);
    }
    //Tourner à gauche :
    if (ir_decode.value == 0xA23D33CC){
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(FORWARD);
    }
    //Tourner à droite :
    if (ir_decode.value == 0xA23D23DC){
      Serial.print("right");
      motor3.run(FORWARD;
      motor4.run(BACKWARD);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~       
    //Tourner à gauche moteur bloqué - avant :
    if (ir_decode.value == 0xA23D31CE){
      Serial.print("left");
      motor3.run(RELEASE);
      motor4.run(FORWARD);
    }
    //Tourner à droite moteur bloqué - avant :
    if (ir_decode.value == 0xA23D11EE){
      Serial.print("right");
      motor3.run(FORWARD);
      motor4.run(RELEASE);
    }
    //Tourner à gauche moteur bloqué - arrière :
    if (ir_decode.value == 0xA23D21DE){
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(RELEASE);
    }
    //Tourner à droite moteur bloqué - arrière :
    if (ir_decode.value == 0xA23D01FE){
      Serial.print("right");
      motor3.run(RELEASE);
      motor4.run(BACKWARD);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    //Virage à gauche avant :
    if (ir_decode.value == 0xA23D7986){
      Serial.print("left");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
      motor4.setSpeed(vitesse/2);
    }
    //Virage à droite avant :
    if (ir_decode.value == 0xA23D59A6){
      Serial.print("right");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
      motor3.setSpeed(vitesse/2);
    }
    //Virage à gauche arrière :
    if (ir_decode.value == 0xA23DB946){
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
      motor4.setSpeed(vitesse/2);
    }
    //Virage à droite arrière :
    if (ir_decode.value == 0xA23D19E6){
      Serial.print("right");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
      motor3.setSpeed(vitesse/2);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~       
    //Vitesse 100 :
    if (ir_decode.value == 0xA23DE21D){
      Serial.print("99");
      motor3.setSpeed(99);
      motor4.setSpeed(99);
      vitesse = 99;
    }
    //Vitesse 150 :
    if (ir_decode.value == 0xA23D02FD){
      Serial.print("149");
      motor3.setSpeed(149);
      motor4.setSpeed(149);
      vitesse = 149;
    }
    //Vitesse 200 :
    if (ir_decode.value == 0xA23D827D){
      Serial.print("199");
      motor3.setSpeed(199);
      motor4.setSpeed(199);
      vitesse = 199;
    }
    //Vitesse 255 :
    if (ir_decode.value == 0xA23D42BD){
      Serial.print("255");
      motor3.setSpeed(255);
      motor4.setSpeed(255);
      vitesse = 255;
    }
    
  }
} 
