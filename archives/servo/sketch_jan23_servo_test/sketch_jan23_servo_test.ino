#define VITESSE 340

const int USTrig = 8; // Déclencheur sur la broche 8
const int USEcho = 9; // Réception sur la broche 9

#include <Servo.h> 
 
Servo servo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 

void setup() {
    pinMode(USTrig, OUTPUT);
    pinMode(USEcho, INPUT);
    servo.attach(6);  // attaches the servo on pin 6 to the servo object 

    digitalWrite(USTrig, LOW);

}

void loop()
{
servo.write(0);
delay(2000);
servo.write(89);
delay(2000);
servo.write(179);
delay(2000);
}

