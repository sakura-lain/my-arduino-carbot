# My Arduino carbot

## My Arduino carbot. Comes with three main programs : remote control, telemeter and night butterfly.

### Remote-control + telemeter scheme:

![](schemes/jpg/carbot-1.jpg "Remote-control + telemeter scheme")

### In-use pins:

![](schemes/jpg/carbot-2.jpg "In use pins")

### A night butterfly with photoresistors:

![](schemes/jpg/carbot-3.jpg "A night butterfly with photoresistors")