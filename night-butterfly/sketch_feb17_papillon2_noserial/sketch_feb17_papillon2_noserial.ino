#include <AFMotor.h>

const int led1 = 10;  
const int led2 = 9;
const char capteur1 = A2;
const char capteur2= A3;
AF_DCMotor motor3(3, MOTOR34_1KHZ);
AF_DCMotor motor4(4, MOTOR34_1KHZ);

int vitesse1 = 0;  
int vitesse2 = 0;

void setup()
{
    // définition des broches utilisées
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    motor3.setSpeed(0);
    motor4.setSpeed(0);
    //Serial.begin(9600); // la voie série pour monitorer
}

void loop()
{
    // conversion de cette valeur en tension
    vitesse1 = (analogRead(capteur1) * 255) / 1024;
    vitesse2 = (analogRead(capteur2) * 255) / 1024;

    if(vitesse1 > vitesse2)
    {
        digitalWrite(led2, HIGH); 
        digitalWrite(led1, LOW); 
        motor3.run(FORWARD);
        motor4.run(FORWARD);
        motor3.setSpeed(vitesse2);
        motor4.setSpeed(vitesse1);
        
        
    }
    if(vitesse2 > vitesse1)
    {
        digitalWrite(led1, HIGH); 
        digitalWrite(led2, LOW);
        motor3.run(FORWARD);
        motor4.run(FORWARD);
        motor3.setSpeed(vitesse2);
        motor4.setSpeed(vitesse1);
    }
    /*else 
    {
        digitalWrite(led2, LOW); 
        digitalWrite(led1, LOW); 
    }*/
    /* envoie de la valeur de la tension lue vers l'ordinateur via la liaison série
    Serial.print("Vitesse 1 = ");
    Serial.println(vitesse1);
    Serial.print("capteur 1 = ");
    Serial.println(capteur1);
    Serial.print("Vitesse 2 = ");
    Serial.println(vitesse2);
    Serial.print("capteur 2 = ");
    Serial.println(capteur2);*/


    // délai pour ne prendre des mesures que toutes les demi-secondes
    delay(10);
}

